package repository

import (
	"context"
	"fmt"
	"main/models"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoDBRepository struct {
	collection *mongo.Collection
}

func NewMongoDBRepository(client *mongo.Client, dbName string, collectionName *mongo.Collection) (VacancyRepository, error) {
	collection := client.Database(dbName).Collection(collectionName.Name())
	return &MongoDBRepository{collection: collection}, nil
}

func NewMongoClient(config Config) (*mongo.Client, error) {
	uri := fmt.Sprintf("mongodb://%s:%s", config.Host, config.Port)
	fmt.Println(uri)
	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	fmt.Println(client)
	if err != nil {
		return nil, fmt.Errorf("failed to create MongoDB client: %v", err)
	}
	ctx := context.Background()
	err = client.Connect(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to connect to MongoDB: %v", err)
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to ping MongoDB: %v", err)
	}

	return client, nil
}

func (v *MongoDBRepository) CreateVacancy(vacancy *models.Vacancy) error {

	id := primitive.NewObjectID()
	idAsInt := int(id.Timestamp().Unix())

	vacancy.ID = idAsInt

	_, err := v.collection.InsertOne(context.Background(), vacancy)
	if err != nil {
		return err
	}
	return nil
}

func (r *MongoDBRepository) GetVacancy(id int) (*models.Vacancy, error) {
	filter := bson.M{"id": id}
	var vacancy models.Vacancy
	err := r.collection.FindOne(context.Background(), filter).Decode(&vacancy)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, fmt.Errorf("no vacancy found for id %d", id)
		}
		return nil, fmt.Errorf("failed to get vacancy: %v", err)
	}
	return &vacancy, nil
}

func (r *MongoDBRepository) UpdateVacancy(id int, vacancy *models.Vacancy) error {
	filter := bson.M{"id": id}
	update := bson.M{
		"$set": bson.M{
			"@context":            vacancy.Context,
			"@type":               vacancy.Type,
			"date_posted":         vacancy.DatePosted,
			"title":               vacancy.Title,
			"description":         vacancy.Description,
			"identifier":          vacancy.Identifier,
			"valid_through":       vacancy.ValidThrough,
			"hiring_organization": vacancy.HiringOrganization,
			"job_location":        vacancy.JobLocation,
			"job_location_type":   vacancy.JobLocationType,
			"employment_type":     vacancy.EmploymentType,
		},
	}
	result, err := r.collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		return fmt.Errorf("failed to update vacancy: %v", err)
	}
	if result.MatchedCount == 0 {
		return fmt.Errorf("no vacancy was updated")
	}
	return nil
}

func (r *MongoDBRepository) DeleteVacancy(id int) error {
	filter := bson.M{"id": id}
	result, err := r.collection.DeleteOne(context.Background(), filter)
	if err != nil {
		return fmt.Errorf("failed to delete vacancy: %v", err)
	}

	if result.DeletedCount == 0 {
		return fmt.Errorf("no vacancy was deleted")
	}

	return nil
}

func (r *MongoDBRepository) GetAllVacancies() ([]*models.Vacancy, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	cursor, err := r.collection.Find(ctx, bson.M{})
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)

	var result []*models.Vacancy

	for cursor.Next(ctx) {
		var v models.Vacancy
		if err := cursor.Decode(&v); err != nil {
			return nil, err
		}
		result = append(result, &v)
	}

	if err := cursor.Err(); err != nil {
		return nil, err
	}

	return result, nil
}
