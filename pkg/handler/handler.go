package handler

import (
	"main/pkg/service"

	_ "main/docs"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
)

type Handler struct {
	services service.VacancyService
}

func NewHandler(service service.VacancyService) *Handler {
	return &Handler{services: service} //services
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.New()

	router.POST("/vacancy", h.createVacancy)
	router.GET("/vacs", h.getAllVacancies)
	router.GET("/vacancy/:id", h.getVacancyById)
	router.PUT("/vacancy/:id", h.updateVacancy)
	router.DELETE("/vacancy/:id", h.deleteVacancy)
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return router
}
