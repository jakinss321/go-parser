package service

import (
	"main/models"
	"main/pkg/repository"
)

type VacancyService interface {
	CreateVacancy(vacancy *models.Vacancy) error
	GetVacancy(id int) (*models.Vacancy, error)
	GetAllVacancies() ([]*models.Vacancy, error)
	UpdateVacancy(id int, vacancy *models.Vacancy) error
	DeleteVacancy(id int) error
}

type VacancyServices struct {
	repo repository.VacancyRepository
}

func NewVacancyService(repo repository.VacancyRepository) VacancyService {
	return &VacancyServices{repo: repo}
}

func (s *VacancyServices) CreateVacancy(vacancy *models.Vacancy) error {
	return s.repo.CreateVacancy(vacancy)
}

func (s *VacancyServices) GetVacancy(id int) (*models.Vacancy, error) {
	return s.repo.GetVacancy(id)
}

func (s *VacancyServices) GetAllVacancies() ([]*models.Vacancy, error) {
	return s.repo.GetAllVacancies()
}

func (s *VacancyServices) UpdateVacancy(id int, vacancy *models.Vacancy) error {
	return s.repo.UpdateVacancy(id, vacancy)
}

func (s *VacancyServices) DeleteVacancy(id int) error {
	return s.repo.DeleteVacancy(id)
}
