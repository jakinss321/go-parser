package main

import (
	"context"
	"log"
	"main/pkg/handler"
	"main/pkg/parsing"
	"main/pkg/repository"
	"main/pkg/service"
	"main/server"
	"os"
	"os/signal"
	"syscall"

	_ "github.com/lib/pq"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/subosito/gotenv"
)

// @title           Documentation of your project API.
// @version         1.0

// @contact.name   API CRUD app

// @host      localhost:8080

func main() {
	logrus.SetFormatter(new(logrus.JSONFormatter))
	if err := initConfig(); err != nil {
		logrus.Fatalf("error initializing configs: %s", err.Error())
	}

	if err := gotenv.Load(); err != nil {
		logrus.Fatalf("error loading env variables: %s", err.Error())
	}

	var db repository.VacancyRepository

	switch os.Getenv("DB") {
	case "postgres":
		cfg := repository.Config{
			Host:     viper.GetString("db.hostpostgres"),
			Port:     viper.GetString("db.postgresport"),
			Username: viper.GetString("db.username"),
			DBName:   viper.GetString("db.dbnamepostgres"),
			SSLMode:  viper.GetString("db.sslmode"),
			Password: os.Getenv("DB_PASSWORD"),
		}
		postgresDB, err := repository.NewPostgresDB(cfg)
		if err != nil {
			log.Fatalf("Failed to initialize db: %s", err.Error())
		}
		db, _ = repository.NewRepository(postgresDB, nil, nil)

	case "mongo":
		cfg := repository.Config{
			Host: viper.GetString("db.hostmongo"),
			Port: viper.GetString("db.port"),
		}
		mongoClient, err := repository.NewMongoClient(cfg)
		if err != nil {
			log.Fatalf("Failed to initialize db: %s", err.Error())
		}
		collection := mongoClient.Database("db-name").Collection(viper.GetString("db.collectionname"))
		db, _ = repository.NewRepository(nil, mongoClient, collection)

	default:
		panic("Unknown database type")
	}

	services := service.NewVacancyService(db)
	handlers := handler.NewHandler(services)

	parsingService := parsing.NewParsing(services)
	go parsingService.Start()
	srv := new(server.Server)
	go func() {
		if err := srv.Run(viper.GetString("port"), handlers.InitRoutes()); err != nil {
			logrus.Fatalf("error occured while running http server: %s", err.Error())
		}
	}()
	logrus.Info("Started")

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	logrus.Info("Shutting Down")

	if err := srv.Shutdown(context.Background()); err != nil {
		logrus.Errorf("error occured on server shutting down: %s", err.Error())
	}

}

func initConfig() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}

// func (p *VacController) VacGetById(w http.ResponseWriter, r *http.Request) {
// 	var (
// 		vac   models.Vacancy
// 		err   error
// 		IDRaw string
// 		ID    int
// 	)

// 	IDRaw = chi.URLParam(r, "ID")

// 	ID, err = strconv.Atoi(IDRaw)
// 	if err != nil {
// 		http.Error(w, err.Error(), http.StatusBadRequest)
// 		return
// 	}

// 	vac, err = p.storage.GetById(ID)
// 	if err != nil {
// 		http.Error(w, err.Error(), http.StatusNotFound)
// 		return
// 	}

// 	w.Header().Set("Content-Type", "application/json;charset=utf-8")
// 	err = json.NewEncoder(w).Encode(vac)

// 	if err != nil {
// 		http.Error(w, err.Error(), http.StatusInternalServerError)
// 		return
// 	}
// }

// func (p *VacController) VacGetList(w http.ResponseWriter, r *http.Request) {
// 	vacs := p.storage.GetList()

// 	w.Header().Set("Content-Type", "application/json;charset=utf-8")
// 	err := json.NewEncoder(w).Encode(vacs)

// 	if err != nil {
// 		http.Error(w, err.Error(), http.StatusInternalServerError)
// 		return
// 	}
// }

// func (p *VacController) VacDelete(w http.ResponseWriter, r *http.Request) {
// 	var (
// 		err   error
// 		IDRaw string
// 		ID    int
// 	)

// 	IDRaw = chi.URLParam(r, "ID")
// 	ID, err = strconv.Atoi(IDRaw)
// 	if err != nil {
// 		http.Error(w, err.Error(), http.StatusBadRequest)
// 		return
// 	}

// 	err = p.storage.Delete(ID)
// 	if err != nil {
// 		http.Error(w, err.Error(), http.StatusNotFound)
// 		return
// 	}

// 	w.WriteHeader(http.StatusNoContent)
// }

// type VacController struct {
// 	storage *repository.Vacancyg
// }

// func NewVacController() *VacController {
// 	return &VacController{storage: repository.NewVacancy()}
// }

// go newServer()
// var caps = selenium.Capabilities{
// 	"browserName": "chrome",
// }
// var chromeCaps = chrome.Capabilities{
// 	Args: []string{
// 		"--headless",
// 	},
// }
// caps.AddChrome(chromeCaps)
// wd, err := selenium.NewRemote(caps, "http://localhost:9515")
// if err != nil {
// 	log.Fatal(err)
// }
