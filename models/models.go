// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    welcome, err := UnmarshalWelcome(bytes)
//    bytes, err = welcome.Marshal()

package models

type Vacancy struct {
	ID                 int         `json:"id" db:"id"`
	Context            string      `json:"@context" db:"context"`
	Type               string      `json:"@type" db:"type"`
	DatePosted         string      `json:"datePosted" db:"date_posted"`
	Title              string      `json:"title" db:"title" binding:"required"`
	Description        string      `json:"description" db:"description"`
	Identifier         interface{} `json:"identifier" db:"identifier"`
	ValidThrough       string      `json:"validThrough" db:"valid_through"`
	HiringOrganization interface{} `json:"hiringOrganization" db:"hiring_organization"`
	JobLocation        interface{} `json:"jobLocation" db:"job_location"`
	JobLocationType    string      `json:"jobLocationType" db:"job_location_type"`
	EmploymentType     string      `json:"employmentType" db:"employment_type"`
}
