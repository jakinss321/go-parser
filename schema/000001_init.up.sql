CREATE TABLE vacancyTable (
    id SERIAL PRIMARY KEY,
    context TEXT NOT NULL,
    type TEXT NOT NULL,
    date_posted TEXT NOT NULL,
    title TEXT NOT NULL,
    description TEXT,
    identifier TEXT,
    valid_through TEXT NOT NULL,
    hiring_organization TEXT,
    job_location TEXT,
    job_location_type TEXT,
    employment_type TEXT
);